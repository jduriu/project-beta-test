from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50, null=True)
    year = models.PositiveSmallIntegerField(null=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.IntegerField(unique=True)

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_show_salesPerson", kwargs={"pk": self.pk})

class PotentialCustomer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.name


class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "sales_record",
        on_delete = models.CASCADE,
    )
    potential_customer = models.ForeignKey(
        PotentialCustomer,
        related_name = "sales_record",
        on_delete = models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name = "sales_record",
        on_delete = models.PROTECT,
    )
    sales_price = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_show_sales_record", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.sales_person}"




