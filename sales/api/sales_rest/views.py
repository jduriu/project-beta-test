from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import SalesPerson, PotentialCustomer, AutomobileVO, SaleRecord
from common.json import ModelEncoder
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "id",
        "sold",
    ]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class PotentialCustomerListEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]


class SaleRecordDetailEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "sales_person",
        "potential_customer",
        "sales_price",
        "id",
    ]
    encoders = {
        "sales_person": SalesPersonListEncoder(),
        "potential_customer": PotentialCustomerListEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
        }


@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(sales_person, encoder=SalesPersonListEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Sales Person cannot be created"}
            )
            response.status_code = 400
            return response


@require_http_methods(["PUT", "DELETE"])
def api_show_sales_person(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonListEncoder,
            safe=False,
        )
    else:
        count, _ = SaleRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_potential_customer(request):
    if request.method == "GET":
        potential_customer = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customer": potential_customer},
            encoder=PotentialCustomerListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            potential_customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(potential_customer, encoder=PotentialCustomerListEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Potential Customer cannot be created"}
            )
            response.status_code = 400
            return response


@require_http_methods(["PUT", "DELETE"])
def api_show_potential_customer(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        PotentialCustomer.objects.filter(id=pk).update(**content)
        potential_customer = PotentialCustomer.objects.get(id=pk)
        return JsonResponse(
            potential_customer,
            encoder=PotentialCustomerListEncoder,
            safe=False,
        )
    else:
        count, _ = SaleRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_sale_record(request, employee_id=None):
    if request.method == "GET":
        if employee_id is not None:
            sale_record = SaleRecord.objects.filter(sales_person=employee_id)
        else:
            sale_record = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_record": sale_record},
            encoder=SaleRecordDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content["automobile"]
            AutomobileVO.objects.filter(vin=vin).update(sold=True)
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
            employee_number = content["sales_person"]
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            content["sales_person"] = sales_person
            customer_id = content["potential_customer"]
            potential_customer = PotentialCustomer.objects.get(id=customer_id)
            content["potential_customer"] = potential_customer

            sale_record = SaleRecord.objects.create(**content)
            return JsonResponse(
                sale_record,
                encoder=SaleRecordDetailEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sale Record"},
                status=400,
            )

@require_http_methods(["PUT", "DELETE"])
def api_show_sale_record(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        SaleRecord.objects.filter(id=pk).update(**content)
        sale_record = SaleRecord.objects.get(id=pk)
        return JsonResponse(
            sale_record,
            encoder=SaleRecordDetailEncoder,
            safe=False,
        )
    else:
        count, _ = SaleRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_list_automobileVO(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False,
        )

@require_http_methods(["PUT"])
def api_show_automobileVO(request, vin):
    if request.method == "PUT":
        content = json.loads(request.body)
        AutomobileVO.objects.filter(vin=vin).update(**content)
        automobile = AutomobileVO.objects.get(vin=vin)
        return JsonResponse(
            automobile,
            encoder=AutomobileVOEncoder,
            safe=False,
        )


def api_list_unsold_automobiles(request):
    unsold_automobiles = AutomobileVO.objects.filter(sold=False)
    return JsonResponse(
        {"automobiles": unsold_automobiles},
        encoder=AutomobileVOEncoder,
        safe=False,
    )
