from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True, blank=True)
    color = models.CharField(max_length=50, null=True, blank=True)
    year = models.PositiveSmallIntegerField(null=True, blank=True)
    vip = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.IntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})

class ServiceAppointment(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    owner = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    service = models.TextField()
    completed = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
