import React, {useEffect, useState} from 'react';
import { NavLink } from 'react-router-dom';


const VehicleModelForm = () => {

    const [formValues, setFormValues] = useState(
        {
            name: "",
            pictureUrl: "",
            manufacturer: "",
        }
    );

    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            name: "",
            pictureUrl: "",
            manufacturer: "",
        }))
    }


    const handleNameInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            name: event.target.value,
        }));
    };

    const handlePictureUrlInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            pictureUrl: event.target.value,
        }));
    };

    const handleManufacturerInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            manufacturer: event.target.value,
        }));
    };



    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...formValues}
        data.picture_url = data.pictureUrl
        delete data.pictureUrl
        data.manufacturer_id = data.manufacturer
        delete data.manufacturer

        const vehicleModelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(vehicleModelUrl, fetchConfig);
            if (response.ok) {
                setSubmitted(true);
                resetForm();
            }
        } catch (error) {
            console.log("error", error);
        }

    }





    const[manufacturers, setManufacturers] = useState([])
    useEffect(() => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const fetchManufacturers = async () => {
            try {
                const response = await fetch(url);
                const data = await response.json();
                setManufacturers(data.manufacturers)

            } catch (error) {
                console.log("error", error);
            }
        };

        fetchManufacturers();

    }, []);



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                        <NavLink to="/models">
                            <div className="alert alert-success">Vehicle model created successfully!</div>
                        </NavLink>
                    </div>
                    : null}
                    <h1>Create a new vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameInputChange} placeholder="Name" required
                                type="text" name="name" id="name"
                                className="form-control" value={formValues.name}/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlInputChange} placeholder="Picture Url" required
                                type="url" name="pictureUrl" id="pictureUrl"
                                className="form-control" value={formValues.pictureUrl}/>
                            <label htmlFor="pictureUrl">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleManufacturerInputChange} required id="manufacturer"
                                name="manufacturer" className="form-select" value={formValues.manufacturer}>
                            <option value="">Choose a Manufacturer</option>
                                {manufacturers.map(manufacturer => (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>


    )
};

export default VehicleModelForm;
