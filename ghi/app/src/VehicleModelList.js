import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

import './custom.css'

const VehicleModelList = () => {

    const [models, setModels] = useState([])

    useEffect(() => {
        fetchTechnicians();
    }, []);

    async function fetchTechnicians() {
        const url = 'http://localhost:8100/api/models/';
        try {
            const response = await fetch(url);
            const data = await response.json();
            setModels(data.models)
        } catch (error) {
            console.log("error", error);
        }
    };

    return (
        <>
        <div className="pt-3">
            <h1>Vehicle Models</h1>
        </div>
        <div className="py-3">
            <Link to="/models/new">
                <button className="btn btn-primary">Create A Vehicle Model</button>
            </Link>
        </div>
        <table className="table">
            <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Name</th>
                <th>Vehicle Image</th>
            </tr>
            </thead>
            <tbody>
            {models.map(model => {
                return (
                    <tr key={model.id} >
                        <td>{model.manufacturer.name}</td>
                        <td>{model.name}</td>
                        <td><img className="vehicle-img" src={model.picture_url} alt=""/></td>
                    </tr>
                )
            })}
            </tbody>
        </table>
        </>
    )
}

export default VehicleModelList;
