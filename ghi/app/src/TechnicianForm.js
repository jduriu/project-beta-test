import React, {useState} from 'react';


const TechnicianForm = () => {

    const [formValues, setFormValues] = useState(
        {
            name: "",
            employeeNumber: "",
        }
    )

    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            name: "",
            employeeNumber: "",
        }))
    }

    const handleNameInputChange = (event) => {
        event.persist();
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            name: event.target.value,
        }));
    };

    const handleEmployeeNumberInputChange = (event) => {
        event.persist();
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            employeeNumber: event.target.value,
        }));
    };

    const [submitted, setSubmitted] = useState(false);


    const handleSubmit = (event) => {
        event.preventDefault();
        const data = {...formValues};
        data.employee_number = data.employeeNumber
        delete data.employeeNumber

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const createTechnician = async () => {
            try {
                const response = await fetch(technicianUrl, fetchConfig);
                if (response.ok) {
                    setSubmitted(true);
                    resetForm();
                }
            } catch (error) {
                console.log("error", error);
            }
        }

        createTechnician();
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    {submitted ? <div className="alert alert-success">Technician registered!</div> : null}
                    <h1>Register a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameInputChange} placeholder="name" required
                                type="text" name="name" id="name"
                                className="form-control" value={formValues.name}/>
                            <label htmlFor="Name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumberInputChange} placeholder="Employee Number" required
                                type="number" name="employeeNumber" id="employeeNumber"
                                className="form-control" value={formValues.employeeNumber}/>
                            <label htmlFor="Name">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )

};

export default TechnicianForm;
