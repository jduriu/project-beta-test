from django.urls import path

from .views import (
    api_list_sales_person,
    api_list_potential_customer,
    api_list_sale_record,
    api_list_automobileVO,
    api_show_automobileVO,
    api_list_unsold_automobiles,
    api_show_sale_record,
    api_show_sales_person,
    api_show_potential_customer,

)

urlpatterns = [
    path("sales_person/", api_list_sales_person, name="api_list_sales_person"),
    path("sales_person/<int:pk>/", api_show_sales_person, name="api_list_sales_person"),
    path("potential_customer/", api_list_potential_customer, name="api_list_potential_customer"),
    path("potential_customer/<int:pk>/", api_show_potential_customer, name="api_list_potential_customer"),
    path("sale_record/", api_list_sale_record, name="api_list_sale_record"),
    path("sale_record/<int:pk>/", api_show_sale_record, name="api_show_sale_record"),
    path("automobileVO/", api_list_automobileVO, name="api_list_automobileVO"),
    path("sales_person/<int:employee_id>/sales_records/", api_list_sale_record, name="api_list_sale_record"),
    path("automobileVO/<str:vin>/", api_show_automobileVO, name="api_show_automobileVO"),
    path("unsold_automobiles/", api_list_unsold_automobiles, name="api_list_unsold_automobiles"),
]