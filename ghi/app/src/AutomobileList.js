import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const AutomobileList = () => {

    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        fetchAutomobiles();
    }, []);



    async function fetchAutomobiles() {
        const url = 'http://localhost:8100/api/automobiles/';
        try {
            const response = await fetch(url);
            const data = await response.json();
            setAutomobiles(data.autos)
        } catch (error) {
            console.log("error", error);
        }
    }

    return (
        <>
        <div className="pt-3">
            <h1>Vehicle Inventory</h1>
        </div>
        <div className="py-3">
            <Link to="/automobiles/new">
                <button className="btn btn-primary">Add a Vehicle</button>
            </Link>
        </div>
        <table className="table">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
            </tr>
            </thead>
            <tbody>
            {automobiles.map(auto => {
                return (
                <tr key={auto.id}>
                    <td>{auto.vin}</td>
                    <td>{auto.color}</td>
                    <td>{auto.year}</td>
                    <td>{auto.model.name}</td>
                    <td>{auto.model.manufacturer.name}</td>
                </tr>
                )
            })}
            </tbody>
        </table>
        </>
    )
}

export default AutomobileList;
