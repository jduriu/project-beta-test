import React, {useState} from 'react';

const ServiceHistoryList = () => {

    const [appointments, setAppointments] = useState([]);
    const [searchVIN, setSearchVIN] = useState('');

    const handleSearchVinInputChange = (event) => {
        setSearchVIN(event.target.value)
    }


    const handleSearch = async (event) => {
        event.preventDefault();
        setAppointments([])

        const filteredAutomobilesUrl = `http://localhost:8080/api/automobiles/${searchVIN}/appointments`
        const fetchConfig = {
            method: "get",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(filteredAutomobilesUrl, fetchConfig);
        if (response.ok) {
            const filteredAutomobiles = await response.json();
            setAppointments(filteredAutomobiles.service_appointments)
        }
    }

    return (
        <>
        <div className="pt-5">
            <h1>Service History Search Tool</h1>
        </div>
        <form onSubmit={handleSearch} className="d-flex p-4" id="search-bar-vin">
            <input className="form-control me-2" type="search" onChange={handleSearchVinInputChange} value={searchVIN} placeholder="Enter VIN number" name="vin" id="vin"  aria-label="Search" />
            <button className="btn btn-outline-success me-2" type="submit">Search</button>
        </form>
        <table className="table">
            <thead>
            <tr>
                <th>Vin</th>
                <th>Customer name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>VIP</th>
            </tr>
            </thead>
            <tbody>
            {appointments.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{appointment.automobile.vin}</td>
                    <td>{appointment.owner}</td>
                    <td>{appointment.date}</td>
                    <td>{appointment.time}</td>
                    <td>{appointment.technician.name}</td>
                    <td>{appointment.service}</td>
                    <td>{appointment.automobile.vip ? "yes" : "no"}</td>
                </tr>
                )
            })}
            </tbody>
        </table>
        </>
    )

}

export default ServiceHistoryList;
