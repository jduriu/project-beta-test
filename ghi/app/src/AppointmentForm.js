import React, {useEffect, useState} from 'react';
import {NavLink} from 'react-router-dom';

const AppointmentForm = () => {
    const [formValues, setFormValues] = useState(
        {
            vin: "",
            owner: "",
            date: "",
            time: "",
            service: "",
            technician: "",
        }
        )

        function resetForm() {
            setFormValues((entries) => ({
                ...entries,
                vin: "",
                owner: "",
                date: "",
                time: "",
                service: "",
                technician: "",
            }))
        }

        const handleVinInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            vin: event.target.value,
        })
        );
    };

    const handleOwnerInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            owner: event.target.value,
        }));
    };

    const handleDateInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            date: event.target.value,
        }));
    };

    const handleTimeInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            time: event.target.value,
        }));
    };

    const handleServiceInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            service: event.target.value,
        }));
    };

    const handleTechnicianInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            technician: event.target.value,
        }));
    };


    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = {...formValues}

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const createVehicleModel = async () => {
            try {
                const response = await fetch(appointmentUrl, fetchConfig);
                if (response.ok) {
                    setSubmitted(true);
                    resetForm();
                }
            } catch (error) {
                console.log("error", error);
            }
        }

        createVehicleModel();
    }


    const [technicians, setTechnicians] = useState([])
    useEffect(() => {
        const url = 'http://localhost:8080/api/technicians/';

        const fetchTechnicians = async () => {
            try {
                const response = await fetch(url);
                const data = await response.json();
                setTechnicians(data.technicians)
            } catch (error) {
                console.log("error", error);
            }
        };

        fetchTechnicians();

    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6 p-4">
                <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                        <NavLink to="/appointments">
                        <div className="alert alert-success">Appointment booked! Click here to see appointments list</div>
                        </NavLink>
                    </div>
                    : null}
                    <h1>Book a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleVinInputChange} value={formValues.vin} placeholder="vin" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Enter the vehicle VIN number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleOwnerInputChange} placeholder="Owner" required
                                type="text" name="owner" id="owner"
                                className="form-control" value={formValues.owner}/>
                            <label htmlFor="owner">Enter the vehicle owners full name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateInputChange} placeholder="Date" required
                                type="date" name="date" id="date"
                                className="form-control" value={formValues.date}/>
                            <label htmlFor="date">Date of the appointment</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTimeInputChange} placeholder="Time" required
                                type="time" name="time" id="time"
                                className="form-control" value={formValues.time}/>
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechnicianInputChange} required id="technician"
                                name="technician" className="form-select" value={formValues.technician}>
                            <option value="">Choose a Technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_number} value={technician.employee_number}>
                                            {technician.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleServiceInputChange} placeholder="Service" required
                                type="text" name="service" id="service"
                                className="form-control" value={formValues.service}/>
                            <label htmlFor="service">Service description</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default AppointmentForm;
