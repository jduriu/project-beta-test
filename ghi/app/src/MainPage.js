import { Link } from 'react-router-dom';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">AutoPilot</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <p className="lead py-5">
          What department would you like to visit?
        </p>
      </div>
      <div className="container text-center">
        <div className="row">
          <div class="col">
            <Link to="/automobiles/">
                <button className="btn btn-lg btn-secondary">Inventory</button>
            </Link>
          </div>
          <div class="col">
            <Link to="/sale_record/">
                <button className="btn btn-lg btn-secondary">Sales</button>
            </Link>
          </div>
          <div class="col">
            <Link to="/appointments">
                <button className="btn btn-lg btn-secondary">Service Center</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
