import { BrowserRouter, Routes, Route } from 'react-router-dom';

import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelList from './VehicleModelList';
import AutomobileList from './AutomobileList';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileForm from './AutomobileForm';
import SalesPersonForm from './SalesPersonForm';
import PotentialCustomerForm from './PotentialCustomerForm';
import SaleRecordForm from './SaleRecordForm';
import SalesList from './SalesList';
import SalesHistoryList from './SalesHistoryList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentsList';
import ServiceHistoryList from './ServiceHistoryList';
import VehicleModelForm from './VehicleModelForm'

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/* INVENTORY API */}
          <Route path="models">
            <Route path="" element={<VehicleModelList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          {/* SALES API */}
          <Route path="sales_person/new" element={<SalesPersonForm />} />
          <Route path="potential_customer/new" element={<PotentialCustomerForm />} />
          <Route path="sale_record">
            <Route path="" element={<SalesList/>} />
            <Route path="new" element={<SaleRecordForm />} />
          </Route>
          <Route path="sales_history/" element={<SalesHistoryList/>} />
          {/* SERVICE API */}
          <Route path="technician/new" element={<TechnicianForm/>} />
          <Route path="appointments">
            <Route path="" element={<AppointmentList/>} />
            <Route path="new" element={<AppointmentForm/>} />
          </Route>
          <Route path="service_history_search" element={<ServiceHistoryList/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
